/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator,
  AsyncStorage
} from 'react-native';

import * as firebase from 'firebase'
import Signup from './src/pages/signup';
import Account from './src/pages/account';
import Header from './src/components/header';

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyA-3WPcoCIgGSFkq9B6HRCoE5VqfMFZKdg",
  authDomain: "apptemplate-f22cd.firebaseapp.com",
  databaseURL: "https://apptemplate-f22cd.firebaseio.com",
  storageBucket: "apptemplate-f22cd.appspot.com",
  messagingSenderId: "82832010896"
};

const app = firebase.initializeApp(firebaseConfig);

import styles from './src/styles/common-styles.js';

class AppTemplate extends Component {
  constructor(props){
    super(props);
    this.state = {
      component: null,
      loaded: false
    };
  }

  componentWillMount(){

    AsyncStorage.getItem('user_data').then((user_data_json) => {

      let user_data = JSON.parse(user_data_json);
      let component = {component: Signup};
      if(user_data != null){
        app.authWithCustomToken(user_data.token, (error, authData) => {
          if(error){
            this.setState(component);
          }else{
            this.setState({component: Account});
          }
        });
      }else{
        this.setState(component);
      }
    });

  }

  render() {
    if(this.state.component){
      return (
        <Navigator
          initialRoute={{component: this.state.component}}
          configureScene={() => {
            return Navigator.SceneConfigs.FloatFromRight;
          }}
          renderScene={(route, navigator) => {
            if(route.component){
              return React.createElement(route.component, { navigator });
            }
          }}
        />
      );
    }else{
      return (
        <View style={styles.container}>
          <Header text="React Native Firebase Auth" loaded={this.state.loaded} />  
          <View style={styles.body}></View>
        </View>
      );
    }  
  }
}

AppRegistry.registerComponent('AppTemplate', () => AppTemplate);
